package pl.wroc.pwr.ligretto.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import pl.wroc.pwr.ligretto.game.ui.GameGUI;
import pl.wroc.pwr.ligretto.game.ui.GameUI;
import pl.wroc.pwr.ligretto.pile.GamePile;
import pl.wroc.pwr.ligretto.player.AIPlayer;
import pl.wroc.pwr.ligretto.player.HumanPlayer;
import pl.wroc.pwr.ligretto.player.Player;
import pl.wroc.pwr.ligretto.player.strategy.DoNothingStrategy;
import pl.wroc.pwr.ligretto.player.strategy.SimpleHumanStrategy;

// implements singleton pattern
public class Game extends Observable {
    
    private List<Player> players;
    private List<GamePile> gamePiles;
    private GameUI ui;

    public Game() {
        gamePiles = new ArrayList<GamePile>();
        players = new ArrayList<Player>();
        ui = new GameGUI();
    }

    public static void main(String[] args) {
        GameFactory gameFactory = new GameFactory();
        Game game = gameFactory.getGame();

        Player player1 = new HumanPlayer(new SimpleHumanStrategy());
        Player player2 = new AIPlayer(new DoNothingStrategy());
        Player player3 = new AIPlayer(new DoNothingStrategy());
        Player player4 = new AIPlayer(new DoNothingStrategy());

        game.addPlayer(player1);
        game.addPlayer(player2);
        game.addPlayer(player3);
        game.addPlayer(player4);

        game.play();
    }

    public List<GamePile> getGamePiles() {
        return gamePiles;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void play() {
        ui.initialize();
//        System.out.println("Welcome to Ligretto!");
//        
//        // notify players that the game has started
//        setChanged();
//        notifyObservers("The game has been started");
    }

    public void addPlayer(Player player) {
        players.add(player);
        addObserver(player);
    }

}
