package pl.wroc.pwr.ligretto.game.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

public class Board extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private static final int ROWS = 4;
    private static final int COLUMNS = 4;
    
    private static final int BOARD_WIDTH = 200;
    private static final int BOARD_HEIGHT = 200;
    
    public Board() {
        setLayout(new GridLayout(ROWS, COLUMNS));
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        for(int i = 0; i < COLUMNS; ++i) {
            for(int j = 0; j < ROWS; ++j) {
                JButton pile = new JButton();
                pile.setBorder(BorderFactory.createDashedBorder(Color.BLACK));
                add(pile);
            }
        }
        setSize(BOARD_WIDTH, BOARD_HEIGHT);
    }

}
