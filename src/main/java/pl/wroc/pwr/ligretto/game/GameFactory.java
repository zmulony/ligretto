package pl.wroc.pwr.ligretto.game;


public class GameFactory {

    private Game game;
    
    public Game getGame() {
        if(null == game) {
            synchronized(this) {
                if(null == game) {
                    game = new Game();
                }
            }
        }
        return game;
    }
}
