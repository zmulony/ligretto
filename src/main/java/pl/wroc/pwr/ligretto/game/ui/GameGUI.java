package pl.wroc.pwr.ligretto.game.ui;

import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.JFrame;

import pl.wroc.pwr.ligretto.player.ui.PlayerPanel;

public class GameGUI implements GameUI {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 300;
    
    @Override
    public void initialize() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setTitle("Ligretto");
        
        PlayerPanel player1 = new PlayerPanel();
        player1.setLayout(new BoxLayout(player1, BoxLayout.X_AXIS));
        PlayerPanel player2 = new PlayerPanel();
        player2.setLayout(new BoxLayout(player2, BoxLayout.X_AXIS));
        PlayerPanel player3 = new PlayerPanel();
        player3.setLayout(new BoxLayout(player3, BoxLayout.Y_AXIS));
        PlayerPanel player4 = new PlayerPanel();
        player4.setLayout(new BoxLayout(player4, BoxLayout.Y_AXIS));
        
        frame.getContentPane().add(BorderLayout.SOUTH, player1);
        frame.getContentPane().add(BorderLayout.NORTH, player2);
        frame.getContentPane().add(BorderLayout.EAST, player3);
        frame.getContentPane().add(BorderLayout.WEST, player4);

        Board board = new Board();
        frame.getContentPane().add(BorderLayout.CENTER, board);

        frame.setVisible(true);
    }

    
}
