package pl.wroc.pwr.ligretto.pile;

import java.util.ArrayList;
import java.util.Collection;

import pl.wroc.pwr.ligretto.card.Card;

public class Row implements CardPile {

    ArrayList<Card> cards;
    
    public Row() {
        cards = new ArrayList<Card>();
    }

    public Card get(int i) {
        return cards.get(i);
    }
    
    public Card remove(int i) {
        return cards.remove(i);
    }

    public boolean remove(Card c) {
        return cards.remove(c);
    }
    
    @Override
    public Collection<Card> peek() {
        return cards;
    }

    @Override
    public void add(Card c) {
        cards.add(c);
    }

    @Override
    public int size() {
        return cards.size();
    }

}
