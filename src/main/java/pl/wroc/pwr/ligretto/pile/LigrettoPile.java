package pl.wroc.pwr.ligretto.pile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;

import pl.wroc.pwr.ligretto.card.Card;

public class LigrettoPile implements CardPile {

    Stack<Card> cards;
    
    public LigrettoPile() {
        cards = new Stack<Card>();
    }
    
    public Card pop() {
        return cards.pop();
    }
    
    @Override
    public Collection<Card> peek() {
        ArrayList<Card> c = new ArrayList<Card>();
        c.add(cards.peek());
        
        return c;
    }

    @Override
    public void add(Card c) {
        cards.add(c);
    }

    @Override
    public int size() {
        return cards.size();
    }

}
