package pl.wroc.pwr.ligretto.pile;

public interface Shuffleable {

    void shuffle();
}
