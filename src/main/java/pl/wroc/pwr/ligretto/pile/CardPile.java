package pl.wroc.pwr.ligretto.pile;

import java.util.Collection;

import pl.wroc.pwr.ligretto.card.Card;


public interface CardPile {
    
    public void add(Card c);
    public Collection<Card> peek();
    public int size();
}
