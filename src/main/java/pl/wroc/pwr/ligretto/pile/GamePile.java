package pl.wroc.pwr.ligretto.pile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;

import pl.wroc.pwr.ligretto.card.Card;
import pl.wroc.pwr.ligretto.card.Symbol;

public class GamePile implements CardPile{

    Symbol symbol;
    Stack<Card> cards;
    
    public GamePile() {
        cards = new Stack<Card>();
    }
    
    @Override
    public Collection<Card> peek() {
        ArrayList<Card> c = new ArrayList<Card>();
        c.add(cards.peek());
        
        return c;
    }

    @Override
    public void add(Card c) {
        if(symbol == null)
            symbol = c.getSymbol();
        
        if(symbol.equals(c.getSymbol()))
            cards.add(c);
    }

    @Override
    public int size() {
        return cards.size();
    }
}
