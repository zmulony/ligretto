package pl.wroc.pwr.ligretto.pile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import pl.wroc.pwr.ligretto.card.Card;

public class HandPile implements CardPile, Shuffleable {

    ArrayList<Card> cards;

    public HandPile() {
        cards = new ArrayList<Card>();
    }

    @Override
    public Collection<Card> peek() {
        ArrayList<Card> c = new ArrayList<Card>();
        for(int i = 0; i < Math.min(3, cards.size()); ++i)
            c.add(cards.get(i));
        
        return c;
    }

    @Override
    public void add(Card c) {
        cards.add(c);
    }

// Fisher-Yates shuffle or better use Java Collections.shuffle()
    @Override
    public void shuffle() {
//        Random r = new Random();
//        for(int i = cards.size()-1; i >= 1; --i) {
//            Collections.swap(cards, i, r.nextInt(i));
//        }
        Collections.shuffle(cards);
    }

    @Override
    public int size() {
        return cards.size();
    }
}
