package pl.wroc.pwr.ligretto.player.strategy;

import pl.wroc.pwr.ligretto.pile.HandPile;
import pl.wroc.pwr.ligretto.pile.LigrettoPile;
import pl.wroc.pwr.ligretto.pile.Row;

public class SimpleAIStrategy implements PlayerStrategy {

    @Override
    public void executeStrategy(Row row, LigrettoPile ligrettoPile,
            HandPile handPile) {
        // this strategy simply tries to throw away all cards from row and ligretto pile
        System.out.println("Oh, man! With this strategy I'm gonna win this!");
        
        // first check if player has any card with number 1 in row or ligretto
        // if not, check hand pile -> shuffle -> re-check hand pile
        
        // then check all game piles
        // if you find one on top of which 
        // you can throw another card from row or top of ligretto pile
        // just do this
        // repeat this block until no cards in row and ligretto pile
    }

}
