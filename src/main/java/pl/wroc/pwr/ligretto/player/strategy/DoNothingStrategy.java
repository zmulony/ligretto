package pl.wroc.pwr.ligretto.player.strategy;

import pl.wroc.pwr.ligretto.pile.HandPile;
import pl.wroc.pwr.ligretto.pile.LigrettoPile;
import pl.wroc.pwr.ligretto.pile.Row;

public class DoNothingStrategy implements PlayerStrategy {

    @Override
    public void executeStrategy(Row row, LigrettoPile ligrettoPile, 
            HandPile handPile) {
        System.out.println("I'm just gonna do... nothing?");
    }

}
