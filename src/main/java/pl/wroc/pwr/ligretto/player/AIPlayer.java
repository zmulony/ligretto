package pl.wroc.pwr.ligretto.player;

import pl.wroc.pwr.ligretto.player.strategy.PlayerStrategy;

public class AIPlayer extends Player {

    public AIPlayer() {
        super();
    }
    
    public AIPlayer(PlayerStrategy strategy) {
        super(strategy);
    }
}
