package pl.wroc.pwr.ligretto.player.ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

public class PlayerPanel extends JPanel implements ActionListener, PlayerUI {
    
    /**
     * 
     */
    private static final long serialVersionUID = -5974732904385576405L;

    private static final int MAX_ROW_CARDS = 3;
    
    private static final int CARD_WIDTH = 200;
    private static final int CARD_HEIGHT = 200;
    
    private static final int PANEL_WIDTH = 200;
    private static final int PANEL_HEIGHT = 200;
    
    public PlayerPanel() {
        setSize(PANEL_WIDTH, PANEL_HEIGHT);
        initialize();
    }
    
    @Override
    public void initialize() {
        for(int i = 0; i < MAX_ROW_CARDS; i++) {
            JButton rowCard = new JButton();
            // rowCard.setBackground();
            // rowCard.setForeground();
            rowCard.setSize(CARD_WIDTH, CARD_HEIGHT);
            rowCard.addActionListener(this);
            add(rowCard);
        }
        add(Box.createRigidArea(new Dimension(10, 10)));
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JButton ligrettoCard = new JButton();
        // ligrettoCard.setBackground(karta.getKolor());
        // ligrettoCard.setForeground(Color.WHITE);
        ligrettoCard.setSize(CARD_WIDTH, CARD_HEIGHT);
        ligrettoCard.addActionListener(this);
        add(ligrettoCard);
        
        add(Box.createRigidArea(new Dimension(10, 10)));
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        
        JButton hand = new JButton();
        // hand.setBackground(karta.getKolor());
        // hand.setForeground(Color.WHITE);
        hand.setSize(CARD_WIDTH, CARD_HEIGHT);
        hand.addActionListener(this);
        add(hand);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
    }

}
