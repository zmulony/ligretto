package pl.wroc.pwr.ligretto.player;

import pl.wroc.pwr.ligretto.player.strategy.PlayerStrategy;

public class HumanPlayer extends Player {

    public HumanPlayer() {
        super();
    }
    
    public HumanPlayer(PlayerStrategy strategy) {
        super(strategy);
    }
}
