package pl.wroc.pwr.ligretto.player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Observable;
import java.util.Observer;

import pl.wroc.pwr.ligretto.card.Card;
import pl.wroc.pwr.ligretto.card.Color;
import pl.wroc.pwr.ligretto.card.Symbol;
import pl.wroc.pwr.ligretto.pile.HandPile;
import pl.wroc.pwr.ligretto.pile.LigrettoPile;
import pl.wroc.pwr.ligretto.pile.Row;
import pl.wroc.pwr.ligretto.player.strategy.DoNothingStrategy;
import pl.wroc.pwr.ligretto.player.strategy.PlayerStrategy;

public class Player implements Observer, Runnable {

    Symbol symbol;
    Row row;
    LigrettoPile ligrettoPile;
    HandPile handPile;
    
    PlayerStrategy strategy;

    private static final int MAX_ROW_CARDS = 3;
    private static final int MAX_LIGRETTO_CARDS = 10;
    private static final int MAX_HAND_CARDS = 27;

    private static final int MAX_COLOR_CARDS = 10;

    public Player() {
        initializePiles();
        assignCardsToPiles();
        
        // default strategy is set to DoNothing
        strategy = new DoNothingStrategy();
    }
    
    public Player(PlayerStrategy strategy) {
        this();
        setStrategy(strategy);
    }
    
    public void setStrategy(PlayerStrategy strategy) {
        this.strategy = strategy;
    }

    private void initializePiles() {
        row          = new Row();
        ligrettoPile = new LigrettoPile();
        handPile     = new HandPile();
    }

    private void assignCardsToPiles() {
        ArrayList<Card> deck = new ArrayList<Card>();
        
        // Create deck
        for(int i = 1; i <= MAX_COLOR_CARDS; ++i) {
            deck.add(new Card(symbol, Color.RED, i));
            deck.add(new Card(symbol, Color.GREEN, i));
            deck.add(new Card(symbol, Color.BLUE, i));
            deck.add(new Card(symbol, Color.YELLOW, i));
        }
        
        // Shuffle deck
        Collections.shuffle(deck);
        
        // Assign cards to piles
        for(int i = 0; i < MAX_ROW_CARDS; ++i) {
            row.add(deck.get(i));
        }

        for(int i = 0; i < MAX_LIGRETTO_CARDS; ++i) {
            ligrettoPile.add(deck.get(i + MAX_ROW_CARDS));
        }

        for(int i = 0; i < MAX_HAND_CARDS; ++i) {
            handPile.add(deck.get(i + MAX_ROW_CARDS + MAX_LIGRETTO_CARDS));
        }
    }

    public Collection<Card> peekRow() {
        return row.peek();
    }

    public Collection<Card> peekLigrettoPile() {
        return ligrettoPile.peek();
    }

    public Collection<Card> peekHandPile() {
        return handPile.peek();
    }
    
    public int getRowSize() {
        return row.size();
    }
    
    public int getLigrettoPileSize() {
        return ligrettoPile.size();
    }
    
    public int getHandPileSize() {
        return handPile.size();
    }

    @Override
    public void update(Observable arg0, Object arg1) {
        // TODO Auto-generated method stub
        System.out.println("I've been informed about something: " + String.valueOf(arg1));
        if(String.valueOf(arg1).equals("The game has been started")) {
            System.out.println("YAY! Let's play!");
            run();
        }
    }

    @Override
    public void run() {
        strategy.executeStrategy(row, ligrettoPile, handPile);
    }
}
