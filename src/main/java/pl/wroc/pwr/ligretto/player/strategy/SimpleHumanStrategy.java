package pl.wroc.pwr.ligretto.player.strategy;

import java.util.List;
import java.util.Scanner;

import pl.wroc.pwr.ligretto.card.Card;
import pl.wroc.pwr.ligretto.game.GameFactory;
import pl.wroc.pwr.ligretto.pile.GamePile;
import pl.wroc.pwr.ligretto.pile.HandPile;
import pl.wroc.pwr.ligretto.pile.LigrettoPile;
import pl.wroc.pwr.ligretto.pile.Row;

public class SimpleHumanStrategy implements PlayerStrategy {
    
    Scanner scanner;
    
    public SimpleHumanStrategy() {
        scanner = new Scanner(System.in);
    }

    @Override
    public void executeStrategy(Row row, LigrettoPile ligrettoPile, 
            HandPile handPile) {
        System.out.println("Oh, man! With this strategy I'm gonna win this!");
        
        GameFactory gameFactory = new GameFactory();
        
        // Presenting game piles:
        List<GamePile> gamePiles = gameFactory.getGame().getGamePiles();
        System.out.println("Game piles:");
        if(gamePiles.isEmpty()) {
            System.out.println("There are no game piles yet.");
        }
        for(GamePile gamePile : gamePiles) {
            System.out.println(gamePile.peek());
        }
        
        // Presenting player cards:
        System.out.println("My row:");
        for(Card c : row.peek()) {
            System.out.println(c);
        }
        
        System.out.println("My ligretto top card:");
        for(Card c : ligrettoPile.peek()) {
            System.out.println(c);
        }
        
        // Presenting instructions:
        System.out.println("What do I want to do...");
        
        // Waiting for player input:
        String input = scanner.nextLine();
        scanner.close();
    }

}
