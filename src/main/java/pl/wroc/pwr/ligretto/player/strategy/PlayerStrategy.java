package pl.wroc.pwr.ligretto.player.strategy;

import pl.wroc.pwr.ligretto.pile.HandPile;
import pl.wroc.pwr.ligretto.pile.LigrettoPile;
import pl.wroc.pwr.ligretto.pile.Row;

public interface PlayerStrategy {

    void executeStrategy(Row row, LigrettoPile ligrettoPile, 
            HandPile handPile);
}
