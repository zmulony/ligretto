package pl.wroc.pwr.ligretto.card;

public class Card {

    private Symbol symbol;
    private Color  color;
    private int    number;

    public Card(Symbol symbol, Color color, int number) {
        this.symbol = symbol;
        this.color  = color;
        this.number = number;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public Color getColor() {
        return color;
    }

    public int getNumber() {
        return number;
    }
    
    @Override
    public String toString() {
        return "Number: " + number + " Color: " + color.toString();
    }
}
