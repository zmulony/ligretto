package pl.wroc.pwr.ligretto.card;

public enum Symbol {
    SQUARE, CIRCLE, TRIANGLE, HEART
}
