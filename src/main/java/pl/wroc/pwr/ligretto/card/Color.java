package pl.wroc.pwr.ligretto.card;

public enum Color {
    RED, GREEN, BLUE, YELLOW
}
