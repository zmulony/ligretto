package pl.wroc.pwr.ligretto.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.wroc.pwr.ligretto.card.Card;
import pl.wroc.pwr.ligretto.player.Player;

public class PlayerTest {

    private Player player;

    @BeforeClass
    public static void setUpClass() {

    }

    @Before
    public void setUp() {
        player = new Player();
    }

    @After
    public void tearDown() {

    }

    @AfterClass
    public static void tearDownClass() {

    }
    
    @Test
    public void shouldHaveFortyCardsAfterCreation() {
        // given
        final int expectedRowSize = 3;
        final int expectedLigrettoPileSize = 10;
        final int expectedHandPileSize = 27;
        
        // when
        final int actualRowSize = player.getRowSize();
        final int actualLigrettoPileSize = player.getLigrettoPileSize();
        final int actualHandPileSize = player.getHandPileSize();
        
        // then
        assertEquals("Actual player's row size is not equal to expected deck size", expectedRowSize, actualRowSize);
        assertEquals("Actual player's Ligretto pile size is not equal to expected deck size", expectedLigrettoPileSize, actualLigrettoPileSize);
        assertEquals("Actual player's hand pile size is not equal to expected deck size", expectedHandPileSize, actualHandPileSize);
    }

    @Test
    public void shouldBeAbleToPeekRow() {
        // when
        ArrayList<Card> rowCards = (ArrayList<Card>)player.peekRow();
        
        // then
        assertNotNull("Row is null", rowCards);
    }
    
    @Test
    public void shouldBeAbleToPeekLigrettoPile() {
        // when
        ArrayList<Card> ligrettoPeekCards = (ArrayList<Card>)player.peekLigrettoPile();
        
        // then
        assertNotNull("Ligretto is null", ligrettoPeekCards);
    }
    
    @Test
    public void shouldBeAbleToPeekHandPile() {
        // when
        ArrayList<Card> handPilePeekCards = (ArrayList<Card>)player.peekHandPile();
        
        // then
        assertNotNull("Hand pile is null", handPilePeekCards);
    }
}
