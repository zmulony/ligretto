package pl.wroc.pwr.ligretto.pile;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.wroc.pwr.ligretto.card.Card;
import pl.wroc.pwr.ligretto.card.Color;
import pl.wroc.pwr.ligretto.card.Symbol;
import pl.wroc.pwr.ligretto.pile.GamePile;

public class GamePileTest {
    
    private GamePile pile;

    @BeforeClass
    public static void setUpClass() {

    }

    @Before
    public void setUp() {
        pile = new GamePile();
    }

    @After
    public void tearDown() {

    }

    @AfterClass
    public static void tearDownClass() {

    }

    @Test
    public void shouldBeAbleToAddCard() {
        // given
        Card expectedCard = new Card(Symbol.CIRCLE, Color.BLUE, 1);
        
        // when
        pile.add(expectedCard);
        Card actualCard = ((ArrayList<Card>)pile.peek()).get(0);
        
        // then
        assertEquals("Incorrect number of cards", 1, pile.size());
        assertEquals("Newly added card is different from the expected one", expectedCard, actualCard);
    }
    
    @Test
    public void shouldBeAbleToPeekTopCard() {
        // given
        Card expectedCard = new Card(Symbol.CIRCLE, Color.BLUE, 1);
        pile.add(expectedCard);
        
        // when
        ArrayList<Card> peekedCards = (ArrayList<Card>)pile.peek();
        
        // then
        assertEquals("Incorrect number of peeked cards", 1, peekedCards.size());
        assertEquals("Peeked card is different from the expected one", expectedCard, peekedCards.get(0));
    }
}
