package pl.wroc.pwr.ligretto.pile;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.wroc.pwr.ligretto.card.Card;
import pl.wroc.pwr.ligretto.card.Color;
import pl.wroc.pwr.ligretto.card.Symbol;
import pl.wroc.pwr.ligretto.pile.HandPile;

public class HandPileTest {

    private HandPile pile;

    @BeforeClass
    public static void setUpClass() {

    }

    @Before
    public void setUp() {
        pile = new HandPile();
    }

    @After
    public void tearDown() {

    }

    @AfterClass
    public static void tearDownClass() {

    }

    @Test
    public void shouldBeShuffleable() {
        // when
        pile.shuffle();
        
        // then
    }
    
    @Test
    public void shouldBeAbleToAddCard() {
        // given
        Card expectedCard = new Card(Symbol.CIRCLE, Color.BLUE, 1);
        
        // when
        pile.add(expectedCard);
        Card actualCard = ((ArrayList<Card>)pile.peek()).get(0);
        
        // then
        assertEquals("Incorrect number of cards", 1, pile.size());
        assertEquals("Newly added card is different from the expected one", expectedCard, actualCard);
    }
    
    @Test
    public void shouldBeAbleToPeekAtMostThreeCards() {
        // given
        Card firstCard = new Card(Symbol.CIRCLE, Color.BLUE, 1);
        Card secondCard = new Card(Symbol.CIRCLE, Color.RED, 2);
        Card thirdCard = new Card(Symbol.CIRCLE, Color.GREEN, 3);
        Card fourthCard = new Card(Symbol.CIRCLE, Color.YELLOW, 4);
        
        pile.add(firstCard);
        pile.add(secondCard);
        pile.add(thirdCard);
        pile.add(fourthCard);
        
        // when
        ArrayList<Card> peekedCards = (ArrayList<Card>)pile.peek();
        
        // then
        assertEquals("Incorrect number of peeked cards", 3, peekedCards.size());
    }
}
