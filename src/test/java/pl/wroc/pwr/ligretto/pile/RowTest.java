package pl.wroc.pwr.ligretto.pile;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.wroc.pwr.ligretto.card.Card;
import pl.wroc.pwr.ligretto.card.Color;
import pl.wroc.pwr.ligretto.card.Symbol;
import pl.wroc.pwr.ligretto.pile.Row;

public class RowTest {

    private Row row;

    @BeforeClass
    public static void setUpClass() {

    }

    @Before
    public void setUp() {
        row = new Row();
    }

    @After
    public void tearDown() {

    }

    @AfterClass
    public static void tearDownClass() {

    }

    @Test
    public void shouldBeAbleToAddCard() {
        // given
        Card expectedCard = new Card(Symbol.CIRCLE, Color.BLUE, 1);
        
        // when
        row.add(expectedCard);
        Card actualCard = ((ArrayList<Card>)row.peek()).get(0);
        
        // then
        assertEquals("Incorrect number of cards", 1, row.size());
        assertEquals("Newly added card is different from the expected one", expectedCard, actualCard);
    }
    
    @Test
    public void shouldBeAbleToPeekAllCards() {
        // given
        Card firstCard = new Card(Symbol.CIRCLE, Color.BLUE, 1);
        Card secondCard = new Card(Symbol.CIRCLE, Color.GREEN, 2);
        Card thirdCard = new Card(Symbol.CIRCLE, Color.RED, 3);
        
        row.add(firstCard);
        row.add(secondCard);
        row.add(thirdCard);
        
        // when
        ArrayList<Card> peekedCards = (ArrayList<Card>)row.peek();
        
        // then
        assertEquals("Incorrect number of peeked cards", row.size(), peekedCards.size());
    }
}
